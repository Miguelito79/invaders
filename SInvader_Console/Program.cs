﻿using SInvader_Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SInvader_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Emulator emulator = Emulator.Instance;
            emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\cpudiag.bin", 0x100);
            //emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\8080ex1.com", 0x100);
            //emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\8080exer.com", 0x100);            
            //emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\8080exm.com", 0x100);
            //emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\cputest.com", 0x100);
            //emulator.Run(@"C:\Test\spaceinvaders\i8080_Disassembler\Test\tst8080.com", 0x100);
        }
    }
}
